'use strict'

const convertDocuments = require('@antora/document-converter')
const silenceStderr = require('./silence-stderr')

const UNRESOLVED_XREF_RX = /<a href="#([^#">]+)(#[^">]+)?" class="page unresolved(?: [^">]+)?"/g
const UNRESOLVED_XREF_HINT = ' class="page unresolved'

function detectUnresolvedXrefs (contentCatalog, asciidocConfig) {
  const docsWithUnresolvedXrefs = new Map()
  const unsilenceStderr = silenceStderr()
  const navFiles = contentCatalog.getFiles().filter((file) => file.src.family === 'nav')
  navFiles.forEach((nav) => {
    nav.src.family = 'page'
    nav.out = undefined
  })
  convertDocuments(contentCatalog, asciidocConfig).forEach((doc) => {
    if (doc.contents.includes(UNRESOLVED_XREF_HINT)) {
      const unresolvedXrefs = new Set()
      const contents = doc.contents.toString()
      let match
      while ((match = UNRESOLVED_XREF_RX.exec(contents))) {
        // Q: should we report the whole xref or just the target?
        //const [, targetId, hash ] = match
        //unresolvedXrefs.add(targetId)
        unresolvedXrefs.add(match[1])
      }
      if (unresolvedXrefs.size) docsWithUnresolvedXrefs.set(doc, [...unresolvedXrefs])
    }
  })
  navFiles.forEach((nav) => {
    nav.src.family = 'nav'
    delete nav.out
  })
  unsilenceStderr()
  return docsWithUnresolvedXrefs
}

module.exports = detectUnresolvedXrefs
