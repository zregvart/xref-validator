'use strict'

function generateReport (docsWithUnresolvedXrefs) {
  const reportData = []
  const byOrigin = Array.from(docsWithUnresolvedXrefs).reduce((accum, [page, xrefs]) => {
    let origin
    const originData = page.src.origin
    let startPath = ''
    if (page.src.abspath) {
      origin = [
        `worktree: ${page.src.abspath.slice(0, -1 - page.path.length)}`,
        `component: ${page.src.component}`,
        `version: ${page.src.version}`,
      ].join(' | ')
    } else {
      if (originData.startPath) startPath = `${originData.startPath}/`
      origin = [
        `repo: ${originData.url}`,
        `branch: ${originData.branch}`,
        `component: ${page.src.component}`,
        `version: ${page.src.version}`,
      ].join(' | ')
    }
    if (!(origin in accum)) accum[origin] = []
    accum[origin].push({ path: `${startPath}${page.path}`, xrefs })
    return accum
  }, {})
  reportData.push('Unresolved xrefs (grouped by origin):')
  Object.keys(byOrigin)
    .sort()
    .forEach((origin) => {
      reportData.push('')
      reportData.push(origin)
      byOrigin[origin]
        .sort((a, b) => a.path.localeCompare(b.path))
        .forEach(({ path, xrefs }) => {
          xrefs.forEach((xref) => reportData.push(`  path: ${path} | xref: ${xref}`))
        })
    })
  return reportData.join('\n')
}

module.exports = generateReport
