'use strict'
/* Copyright (c) 2018-2019 OpenDevise, Inc.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

const aggregateContent = require('@antora/content-aggregator')
const buildPlaybook = require('@antora/playbook-builder')
const classifyContent = require('@antora/content-classifier')
const detectUnresolvedXrefs = require('./detect-unresolved-xrefs')
const generateReport = require('./generate-report')
const importSiteManifest = require('./import-site-manifest')
const { resolveConfig: resolveAsciiDocConfig } = require('@antora/asciidoc-loader')

async function generateSite (args, env) {
  const playbook = buildPlaybook(args, env)
  const asciidocConfig = resolveAsciiDocConfig(playbook)
  const contentCatalog = await aggregateContent(playbook).then((contentAggregate) =>
    classifyContent(playbook, contentAggregate, asciidocConfig)
  )
  const primarySiteUrl = asciidocConfig.attributes['primary-site-url']
  const primarySiteManifestUrl = asciidocConfig.attributes['primary-site-manifest-url']
  if (primarySiteUrl || primarySiteManifestUrl) {
    await importSiteManifest(playbook, contentCatalog, primarySiteManifestUrl, primarySiteUrl)
  }
  const docsWithUnresolvedXrefs = detectUnresolvedXrefs(contentCatalog, asciidocConfig)
  if (docsWithUnresolvedXrefs.size) {
    const totalUnresolved = [].concat(...docsWithUnresolvedXrefs.values()).length
    console.error(generateReport(docsWithUnresolvedXrefs))
    console.error(
      [
        '\nantora: xref validation failed!',
        `Found ${totalUnresolved} unresolved xref${totalUnresolved > 1 ? 's' : ''}.`,
        'See previous report for details.',
      ].join(' ')
    )
    process.exitCode = 1
  }
}

module.exports = generateSite
