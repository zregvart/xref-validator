'use strict'

function silenceStderr () {
  const stderrWriter = process.stderr.write
  process.stderr.write = () => {}
  return () => {
    process.stderr.write = stderrWriter
  }
}

module.exports = silenceStderr
