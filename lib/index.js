'use strict'

/**
 * Antora xref validator
 *
 * A custom Antora site generator that performs rudimentary validation of xrefs (specifically page references).
 */
module.exports = require('./generate-site')
